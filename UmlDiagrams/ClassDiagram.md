```mermaid
classDiagram
class ITable {
    <<interface>>
    +TableDescriptor TableDescriptor
    +Get(Record searchDescriptor) IEnumerable~Record~
    +Add(IEnumerable<Record> records)
    +Delete(Record record)
    +Edit(Record searchDescriptor, Record records)
} 

class Table {
    -IRecordIO recordIO
    -TableDescriptor tableSecriptor
    -List~Record~ records
}

class TableDescriptor {
    -string name
    -List~ColumnDescriptor~ columnDescriptor
    +IsRecordValid(Record) bool
}

class IRecordSerializer~TResult~ {
    <<interface>>
    +Serialize(Record record) TResult
    +Deserialize(TResult source) Result 
}

class JsonRecordSerializer {
    
}

class Record {
    +Cell[] cels
    +FromObject(T obj)$ Record
}

class Cell {
    +ColumnDescriptor columnDescriptor
}

class DataBase {
    +GetTable(string name) Table
    +AddTable(TableDescriptor tableDescriptor)
    +DeleteTable(string name)
    +CreateFromPath(string path) DataBase
}

class ColumnDescriptor {
    +TypeEnum type
    +string Name
}

class TypeEnum {
    <<enum>>
}

class IRecordsIO~TRecord~ {
    <<inteface>>
    +Get(uint index) Record
    +Add(Record record)
    +Delete(uint index)
    +Edit(uint index, Record record)
}

class JsonRecordIO {
    
}

DataBase --> IRecordsIO
DataBase --> ITable
ITable --> Record
IRecordSerializer --> Record
Table ..|> ITable
Table --> TableDescriptor
Table --> Record
TableDescriptor --> ColumnDescriptor
Record --* Cell
Cell --> TypeEnum
Cell --> ColumnDescriptor
ColumnDescriptor --> TypeEnum


JsonRecordIO --> JsonRecordSerializer
JsonRecordIO ..|> IRecordsIO
JsonRecordSerializer --> TypeEnum
JsonRecordSerializer --> Cell
JsonRecordSerializer --> ColumnDescriptor
JsonRecordSerializer ..|> IRecordSerializer: string
```
