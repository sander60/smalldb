namespace Sander.SmallDB.UnitTests;

using System.Text.Json.Nodes;

using NFluent;

using NUnit.Framework;

using Sander.SmallDB.Adapters.Json;
using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Services;

[TestFixture]
public class JsonSerilizerTest
{
    private IRecordSerilizer<JsonObject>
        _recordSerilizer = new JsonRecordSerializer();

    class TestClass1
    {
        public string? FirstName { get; set; }

        public string? SecondName { get; set; }

        public int Age { get; set; }
    }

    class TestClass2
    {
        public int X { get; set; }

        public int Y { get; set; }
    }

    [Test]
    public void Test1()
    {
        var originalObject =
            new TestClass1 {
                FirstName = "Bob",
                SecondName = "Smith",
                Age = 33
            };
        var recordOfOriginalObject = new Record(originalObject);
        var jsonObjectOfOriginalObject =
            _recordSerilizer.Serialize(recordOfOriginalObject);
        var deserializedRecord =
            _recordSerilizer.Deserialize(jsonObjectOfOriginalObject);
        var deserializedObject = deserializedRecord.ToType<TestClass1>();

        Check
            .That(deserializedObject)
            .HasFieldsWithSameValues(originalObject);
    }

    [Test]
    public void Test2()
    {
        var originalObject = new TestClass2 { X = 34, Y = -7 };
        var recordOfOriginalObject = new Record(originalObject);
        var jsonObjectOfOriginalObject =
            _recordSerilizer.Serialize(recordOfOriginalObject);
        var deserializedRecord =
            _recordSerilizer.Deserialize(jsonObjectOfOriginalObject);
        var deserializedObject = deserializedRecord.ToType<TestClass2>();

        Check
            .That(deserializedObject)
            .HasFieldsWithSameValues(originalObject);
    }
}