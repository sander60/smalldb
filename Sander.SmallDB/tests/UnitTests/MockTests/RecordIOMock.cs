using Moq;
using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Services;
using System.Collections.Generic;
using System.Text.Json.Nodes;

public class RecordsIOMock<TResult> : Mock<IRecordsIO<TResult>> where TResult : JsonNode
{
    private Dictionary<TableDescriptor, List<Record>> _tableRecords;

    public RecordsIOMock()
    {
        _tableRecords = new Dictionary<TableDescriptor, List<Record>>();
        SetupAllMethods();
    }

    private void SetupAllMethods()
    {
        SetupGet();
        SetupAdd();
        SetupDelete();
        SetupEdit();
        SetupFind();
        SetupTableRecordsEnumerator();
        SetupAddTable();
        SetupDeleteTable();
        SetupRenameTable();
        SetupBind();
    }

    private void SetupGet()
    {
        Setup(io => io.Get(It.IsAny<uint>(), It.IsAny<TableDescriptor>()))
            .Returns<uint, TableDescriptor>((index, tableDescriptor) =>
            {
                var records = GetRecords(tableDescriptor);
                return records[(int)index];
            });
    }

    private void SetupAdd()
    {
        Setup(io => io.Add(It.IsAny<Record>(), It.IsAny<TableDescriptor>()))
            .Callback<Record, TableDescriptor>((record, tableDescriptor) =>
            {
                var records = GetRecords(tableDescriptor);
                records.Add(record);
            });
    }

    private void SetupDelete()
    {
        Setup(io => io.Delete(It.IsAny<uint>(), It.IsAny<TableDescriptor>()))
            .Callback<uint, TableDescriptor>((index, tableDescriptor) =>
            {
                var records = GetRecords(tableDescriptor);
                records.RemoveAt((int)index);
            });
    }

    private void SetupEdit()
    {
        Setup(io => io.Edit(It.IsAny<uint>(), It.IsAny<Record>(), It.IsAny<TableDescriptor>()))
            .Callback<uint, Record, TableDescriptor>((index, record, tableDescriptor) =>
            {
                var records = GetRecords(tableDescriptor);
                records[(int)index] = record;
            });
    }

    private void SetupFind()
    {
        Setup(io => io.Find(It.IsAny<Record>(), It.IsAny<TableDescriptor>()))
            .Returns<Record, TableDescriptor>((record, tableDescriptor) =>
            {
                var records = GetRecords(tableDescriptor);
                return (uint)records.FindIndex(r => r == record);
            });
    }

    private void SetupTableRecordsEnumerator()
    {
        Setup(io => io.TableRecordsEnumerator(It.IsAny<TableDescriptor>()))
            .Returns<TableDescriptor>(tableDescriptor =>
            {
                var records = GetRecords(tableDescriptor);
                return records;
            });
    }

    private void SetupAddTable()
    {
        Setup(io => io.AddTable(It.IsAny<TableDescriptor>()))
            .Callback<TableDescriptor>(tableDescriptor =>
            {
                if (!_tableRecords.ContainsKey(tableDescriptor))
                    _tableRecords.Add(tableDescriptor, new List<Record>());
            });
    }

    private void SetupDeleteTable()
    {
        Setup(io => io.DeleteTable(It.IsAny<TableDescriptor>()))
            .Callback<TableDescriptor>(tableDescriptor =>
            {
                _tableRecords.Remove(tableDescriptor);
            });
    }

    private void SetupRenameTable()
    {
        Setup(io => io.RenameTable(It.IsAny<TableDescriptor>(), It.IsAny<string>()))
            .Callback<TableDescriptor, string>((tableDescriptor, newName) =>
            {
                tableDescriptor.Name = newName;
            });
    }

    private void SetupBind()
    {
        Setup(io => io.Bind(It.IsAny<SchemeDescriptor>()))
            .Callback<SchemeDescriptor>(schemeDescriptor =>
            {
                // No implementation needed for mocking
            });
    }

    private List<Record> GetRecords(TableDescriptor tableDescriptor)
    {
        if (_tableRecords.ContainsKey(tableDescriptor))
            return _tableRecords[tableDescriptor];
        
        var records = new List<Record>();
        _tableRecords.Add(tableDescriptor, records);
        return records;
    }
}
