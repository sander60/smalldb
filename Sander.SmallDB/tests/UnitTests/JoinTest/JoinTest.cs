namespace Sander.SmallDB.UnitTests;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.Json.Nodes;

using NFluent;

using NUnit.Framework;

using Sander.SmallDB.Adapters.Json;
using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Services;
using Sander.SmallDB.Implementations.SmallScheme;
using Sander.SmallDB.Implementations.SmallTable;

[TestFixture]
public class JoinTest
{
    const string DB_NAME = "DbTest1";

    const string DB_PATH = "Resources";

    const string USERS_TABLE_NAME = "Users";

    const string BOOKS_TABLE_NAME = "Books";

    [Test]
    public void TestJoin()
    {
        var db = CreateEmptyDataBase();
        CreateTables(db);
        AddRecords(db);

        var joinResult = db.Join(
            db.GetTable(USERS_TABLE_NAME),
            db.GetTable(BOOKS_TABLE_NAME),
            "Name",
            "AuthorName");
        Directory.Delete(DB_PATH, true);
        Check.That(joinResult).HasSize(2);
        foreach(var r in joinResult)
        {
            var name = r.Cells.First(c => c.ColumnDescriptor.Name == "Name").Value;
            var book = r.Cells.First(c => c.ColumnDescriptor.Name == "Title").Value;

            if ( name == "John" && book != "Math")
            {
                Assert.Fail();
            }
            else if (name == "Alex" && book != "Biology")
            {
                Assert.Fail();
            }
        }
    }

    private IScheme<JsonObject> CreateEmptyDataBase()
    {
        var recordsIO = new JsonRecordsIO();
        var tableFactory = new SmallTableFactory<JsonObject>();
        var db = new SmallScheme<JsonObject>(DB_PATH, DB_NAME, recordsIO, tableFactory);

        return db;
    }

    private void CreateTables(IScheme<JsonObject> db)
    {
        db.AddTable(new TableDescriptor(USERS_TABLE_NAME, typeof(User)));
        db.AddTable(new TableDescriptor(BOOKS_TABLE_NAME, typeof(Book)));
    }

    private void AddRecords(IScheme<JsonObject> dataBase)
    {
        // Users
        var Users = new[] 
            {
                new User("Bob", "Smith", 33, new MailAddress("Bob.Smith@gmail.com")),
                new User("John", "Snow", 25, new MailAddress("John.Snow@mail.net")),
            };
        dataBase.GetTable(USERS_TABLE_NAME)?.Add(Users.Select(x => new Record(x)));
        

        // Books
        var Books = new[] 
            {
                new Book("Math", "John", 59.99),
                new Book("Biology", "Bob", 45.99),
            };
        dataBase.GetTable(BOOKS_TABLE_NAME)?.Add(Books.Select(x => new Record(x)));
        
    }

    private class User
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public int? Age { get; set; }

        public MailAddress MailAddress { get; set; }

        public User(string name, string surname, int? age, MailAddress mailAddress)
        {
            Name = name;
            Surname = surname;
            Age = age;
            MailAddress = mailAddress;
        }
    }

    private class Book
    {
        public string Title { get; set; }

        public string AuthorName { get; set; }

        public double? Price { get; set; }

        public Book(string title, string author, double? price)
        {
            Title = title;
            AuthorName = author;
            Price = price;
        }
    }
}