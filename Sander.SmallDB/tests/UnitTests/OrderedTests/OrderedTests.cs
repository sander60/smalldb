using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.Json.Nodes;

using NUnit.Framework;

using Sander.SmallDB.Adapters.Json;
using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Models.Exceptions;
using Sander.SmallDB.Contracts.Services;
using Sander.SmallDB.Implementations.SmallScheme;
using Sander.SmallDB.Implementations.SmallTable;

namespace Sander.SmallDB.UnitTests;

[TestFixture]
public class DatabaseTests
{
    private const string DB_TEST_NAME = "DbTest1";
    private const string DB_TEST_PATH = "Resources";

    private string _dbPath;
    private IScheme<JsonObject> _db;

    [OneTimeSetUp]
    public void Setup()
    {
        _dbPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DB_TEST_PATH);
        if (!Directory.Exists(_dbPath))
            Directory.CreateDirectory(_dbPath);

        _db = CreateEmptyDataBase();
    }

    [OneTimeTearDown]
    public void Cleanup()
    {
        if (Directory.Exists(_dbPath))
            Directory.Delete(_dbPath, true);
    }

    [Test, Order(1)]
    public void CreateEmptyDataBase_ShouldCreateDatabase()
    {
        var recordsIO = new JsonRecordsIO();
        var tableFactory = new SmallTableFactory<JsonObject>();
        _db = new SmallScheme<JsonObject>(_dbPath, DB_TEST_NAME, recordsIO, tableFactory);

        Assert.IsNotNull(_db);
    }

    [Test, Order(2)]
    public void CreateTables_ShouldAddTablesToDatabase()
    {
        var usersTable = new TableDescriptor("Users", typeof(User));
        var booksTable = new TableDescriptor("Books", typeof(Book));

        _db.AddTable(usersTable);
        _db.AddTable(booksTable);

        Assert.AreEqual(2, _db.SchemeDescriptor.TableDescriptors.Count());
    }

    [Test, Order(3)]
    public void AddRecords_ShouldAddRecordsToTables()
    {
        var usersTable = _db.GetTable("Users");
        var booksTable = _db.GetTable("Books");

        var users = new[]
        {
            new User("Bob", "Smith", 33, new MailAddress("Bob.Smith@gmail.com")),
            new User("John", "Snow", 25, new MailAddress("John.Snow@mail.net")),
        };

        var books = new[]
        {
            new Book("Math", "John", 59.99),
            new Book("Biology", "Bob", 45.99),
        };

        usersTable.Add(users.Select(x => new Record(x)));
        booksTable.Add(books.Select(x => new Record(x)));

        Assert.AreEqual(2, usersTable.GetAll().Count());
        Assert.AreEqual(2, booksTable.GetAll().Count());
    }

    [Test, Order(4)]
    public void ChangeUserName_ShouldUpdateUserName()
    {
        var usersTable = _db.GetTable("Users");
        var userToChange = new User("Bob", "Smith", 33, new MailAddress("Bob.Smith@gmail.com"));
        var updatedUser = new User("Alex", "Smith", 45, new MailAddress("alex.Smith@mail.net"));

        usersTable.Delete(new Record(userToChange));
        usersTable.Edit(new Record(updatedUser), new Record(new User("John", null, null, null)));

        var updatedUserRecord = usersTable.GetAll().FirstOrDefault(u => u.ToType<User>().Name == "Alex");

        Assert.IsNotNull(updatedUserRecord);
        Assert.AreEqual("Alex", updatedUserRecord.ToType<User>().Name);
    }

    [Test, Order(5)]
    public void DeleteTable_ShouldRemoveTableFromDatabase()
    {
        _db.DeleteTable("Books");

        TestDelegate act = () =>
        {
            _db.GetTable("Books");
        };

        Assert.Throws<TableException>(act);
    }

    private IScheme<JsonObject> CreateEmptyDataBase()
    {
        var recordsIO = new JsonRecordsIO();
        var tableFactory = new SmallTableFactory<JsonObject>();
        return new SmallScheme<JsonObject>(_dbPath, DB_TEST_NAME, recordsIO, tableFactory);
    }

}

class User
{
    public string Name { get; set; }

    public string Surname { get; set; }

    public int? Age { get; set; }

    public MailAddress MailAddress { get; set; }

    public User(string name, string surname, int? age, MailAddress mailAddress)
    {
        Name = name;
        Surname = surname;
        Age = age;
        MailAddress = mailAddress;
    }

    public User() { }
}

class Book
{
    public string Title { get; set; }

    public string AuthorName { get; set; }

    public double? Price { get; set; }

    public Book(string title, string author, double? price)
    {
        Title = title;
        AuthorName = author;
        Price = price;
    }
}