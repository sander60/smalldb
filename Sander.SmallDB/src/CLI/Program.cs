﻿using System.Net.Mail;
using System.Text.Json.Nodes;

using Sander.SmallDB.Adapters.Json;
using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Services;
using Sander.SmallDB.Implementations.SmallScheme;
using Sander.SmallDB.Implementations.SmallTable;

const string DB_NAME = "DbTest1";

const string DB_PATH = "Resources";

const string USERS_TABLE_NAME = "Users";

const string BOOKS_TABLE_NAME = "Books";


var db = CreateEmptyDataBase();
CreateTables(db);
PrintTables(db);
Console.ReadKey();
AddRecords(db);
PrintTables(db);
Console.ReadKey();
PrintRecords(db.Join(
    db.GetTable(USERS_TABLE_NAME),
    db.GetTable(BOOKS_TABLE_NAME),
    "Name",
    "AuthorName"));
Console.ReadKey();
ChangeUserName(db);
PrintTables(db);
Console.ReadKey();
db.DeleteTable(BOOKS_TABLE_NAME);
PrintTables(db);
Console.ReadKey();

IScheme<JsonObject> CreateEmptyDataBase()
{
    var recordsIO = new JsonRecordsIO();
    var tableFactory = new SmallTableFactory<JsonObject>();
    var db = new SmallScheme<JsonObject>(DB_PATH, DB_NAME, recordsIO, tableFactory);

    return db;
}

void CreateTables(IScheme<JsonObject> db)
{
    db.AddTable(new TableDescriptor(USERS_TABLE_NAME, typeof(User)));
    db.AddTable(new TableDescriptor(BOOKS_TABLE_NAME, typeof(Book)));
}

void PrintTable(ITable table)
{
    var tableDescriptor = table.TableDescriptor;

    Console.WriteLine(string.Join("|", tableDescriptor.ColumnDescriptors.Select(x => $"{x.Name,-20}").ToArray())); 
    foreach(var record in table.GetAll())
    {
        Console.WriteLine(string.Join("|", record.Cells.Select(x => $"{x.Value.ToString(),-20}").ToArray())); 
    }
}

void PrintTables(IScheme<JsonObject> dataBase)
{
    Console.Clear();
    Console.WriteLine(USERS_TABLE_NAME + "-----------------------------------");
    PrintTable(dataBase.GetTable(USERS_TABLE_NAME));
    try
    {
    Console.WriteLine(BOOKS_TABLE_NAME + "-----------------------------------");
    PrintTable(dataBase.GetTable(BOOKS_TABLE_NAME));
    }
    catch(Exception e)
    {
        
    }
}

void PrintRecords(IEnumerable<Record> records)
{
    Console.Clear();
    Console.WriteLine(string.Join("|", 
        records
            .First()
            .Cells
            .Select(x =>  $"{x.ColumnDescriptor.Name,-20}").ToArray())); 
    foreach(var record in records)
    {
        Console.WriteLine(string.Join("|", record.Cells.Select(x => $"{x.Value.ToString(),-20}").ToArray())); 
    }
}

void AddRecords(IScheme<JsonObject> dataBase)
{
    // Users
    var Users = new[] 
        {
            new User("Bob", "Smith", 33, new MailAddress("Bob.Smith@gmail.com")),
            new User("John", "Snow", 25, new MailAddress("John.Snow@mail.net")),
        };
    dataBase.GetTable(USERS_TABLE_NAME)?.Add(Users.Select(x => new Record(x)));
    

    // Books
    var Books = new[] 
        {
            new Book("Math", "John", 59.99),
            new Book("Biology", "Bob", 45.99),
        };
    dataBase.GetTable(BOOKS_TABLE_NAME)?.Add(Books.Select(x => new Record(x)));
    
}

void ChangeUserName(IScheme<JsonObject> dataBase)
{
    dataBase.GetTable(USERS_TABLE_NAME).Delete(new Record(new User("Bob", "Smith", 33, new MailAddress("Bob.Smith@gmail.com"))));
    dataBase.GetTable(USERS_TABLE_NAME)
        .Edit(new Record(new User("Alex", "Smith", 45, new MailAddress("alex.Smith@mail.net"))),
            new Record(new User("John", null, null, null)));
}

class User
{
    public string Name { get; set; }

    public string Surname { get; set; }

    public int? Age { get; set; }

    public MailAddress MailAddress { get; set; }

    public User(string name, string surname, int? age, MailAddress mailAddress)
    {
        Name = name;
        Surname = surname;
        Age = age;
        MailAddress = mailAddress;
    }
}

class Book
{
    public string Title { get; set; }

    public string AuthorName { get; set; }

    public double? Price { get; set; }

    public Book(string title, string author, double? price)
    {
        Title = title;
        AuthorName = author;
        Price = price;
    }
}