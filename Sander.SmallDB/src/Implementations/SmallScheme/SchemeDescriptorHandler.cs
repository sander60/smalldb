namespace Sander.SmallDB.Implementations.SmallScheme;

using System.Text;
using Newtonsoft.Json;
using Sander.SmallDB.Contracts.Models;

internal static class SchemeDescriptorHandler 
{
    public const string SchemeDescriptorFileName = "schemeDescriptor.json";

    public static void WriteShemeDescriptor(SchemeDescriptor schemeDescriptor)
    {
        var file = File.Open(
            Path.Combine(schemeDescriptor.DBPath, SchemeDescriptorFileName),
            FileMode.Create);

        var jsonString = GetJsonObject(schemeDescriptor);
        file.Write(Encoding.UTF8.GetBytes(jsonString));
        file.Close();
    }

    public static SchemeDescriptor ReadSchemeDescriptor(string path)
    {
        if (path == null) 
        {
            throw new FileNotFoundException();
        }

        var jsonString = File.ReadAllText(Path.Combine(path, SchemeDescriptorFileName));

        var result = JsonConvert
            .DeserializeObject<SchemeDescriptor>(jsonString) ?? throw new JsonException();
        result.DBPath = path;

        return result;
    }

    private static string GetJsonObject(SchemeDescriptor schemeDescriptor)
    {
        return JsonConvert.SerializeObject(schemeDescriptor);
    }
}