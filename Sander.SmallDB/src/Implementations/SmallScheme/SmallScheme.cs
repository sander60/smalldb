namespace Sander.SmallDB.Implementations.SmallScheme;

using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Models.Exceptions;
using Sander.SmallDB.Contracts.Services;

public class SmallScheme<TRecord> : IScheme<TRecord>
{
    private IRecordsIO<TRecord> _recordsIO;

    private List<ITable> _tables;

    private SchemeDescriptor _schemeDescriptor;

    private ITableFactory<TRecord> _tableFactory;

    public SchemeDescriptor SchemeDescriptor => _schemeDescriptor;

    public SmallScheme(string path, string name, IRecordsIO<TRecord> recordsIO, ITableFactory<TRecord> tableFactory)
    {
        var dbPath = Path.Combine(path, name);
        if (!Directory.Exists(dbPath))
        {
            CreateEmptyDataBase(dbPath);
            _schemeDescriptor = new SchemeDescriptor(name, dbPath);
            SchemeDescriptorHandler.WriteShemeDescriptor(_schemeDescriptor);
        }
        else
        {
            _schemeDescriptor = SchemeDescriptorHandler.ReadSchemeDescriptor(dbPath);
        }
        
        _recordsIO = recordsIO;
        _recordsIO.Bind(_schemeDescriptor);
        _tables = new List<ITable>();
        _tableFactory = tableFactory;

        foreach(var tableDescriptor in _schemeDescriptor.TableDescriptors)
        {
            _tables.Add(_tableFactory.Create(tableDescriptor, _recordsIO));
        }
    }

    public void AddTable(TableDescriptor tableDescriptor)
    {
        if(_schemeDescriptor.TableDescriptors.Where(x =>
            string.Equals(x.Name, tableDescriptor.Name, StringComparison.OrdinalIgnoreCase)).Any())
        {
            throw new TableException("Table alredy exist");
        }
        _recordsIO.AddTable(tableDescriptor);
        _tables.Add(_tableFactory.Create(tableDescriptor, _recordsIO));
        UpdateSchemeDescriptorFile();
    }

    public IEnumerable<Record> Join(ITable left, ITable right, string leftKey, string rightString)
    {
        var leftRecords = _recordsIO.TableRecordsEnumerator(left.TableDescriptor).ToArray();
        var rightRecords = _recordsIO.TableRecordsEnumerator(right.TableDescriptor).ToArray();
    
        var result = leftRecords
            .Join(
                rightRecords,
                l => GetCellByName(l, leftKey),
                r => GetCellByName(r, rightString),
                (l, r) => new Record(l.Cells.Concat(r.Cells.ToArray()).ToArray()))
            .ToArray();

        object GetCellByName(Record record, string name)
        {
            var res = record.Cells.FirstOrDefault(c => 
                    string.Equals(c.ColumnDescriptor.Name, name, StringComparison.OrdinalIgnoreCase))
                ?? throw new TableException("No such key");
            return res.Value;
        }

        return result;
    }

    public void ChangeTableName(string before, string after)
    {
        var table = GetTable(before);

        _recordsIO.RenameTable(table.TableDescriptor, after);

        UpdateSchemeDescriptorFile();
    }

    public void ChangeSchemeName(string name)
    {
        var path = SchemeDescriptor.DBPath;
        SchemeDescriptor.Name = name;
        SchemeDescriptorHandler.WriteShemeDescriptor(_schemeDescriptor);
        Directory.Move(SchemeDescriptor.DBPath, Path.Combine(Path.GetDirectoryName(path), $"{name}.json"));
    }

    public void DeleteTable(string name)
    {
        var table = GetTable(name);
        _tables.Remove(table);
        _recordsIO.DeleteTable(table.TableDescriptor);

        UpdateSchemeDescriptorFile();
    }

    public ITable GetTable(string name)
    {
        return _tables
            .FirstOrDefault(x => 
                string.Equals(x.TableDescriptor.Name, name, StringComparison.OrdinalIgnoreCase))
            ?? throw new TableException("No Such Table");
    }

    private void CreateEmptyDataBase(string path)
    {
        Directory.CreateDirectory(path);
    }

    private void UpdateSchemeDescriptorFile()
    {
        SchemeDescriptorHandler.WriteShemeDescriptor(_schemeDescriptor);
    }
}