namespace Sander.SmallDB.Implementations.SmallTable;

using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Services;

public class SmallTableFactory<TRecord> : ITableFactory<TRecord>
{
    public SmallTableFactory()
    {

    }

    public ITable Create(TableDescriptor tableDescriptor, IRecordsIO<TRecord> recordsIO)
    {
        return new SmallTable<TRecord>(tableDescriptor, recordsIO);
    }
}