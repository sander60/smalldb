﻿namespace Sander.SmallDB.Implementations.SmallTable;

using System.Collections.Generic;

using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Models.Exceptions;
using Sander.SmallDB.Contracts.Services;

public class SmallTable<TRecord> : ITable
{
    public TableDescriptor _tableDescriptor;

    public IRecordsIO<TRecord> _recordsIO;

    public TableDescriptor TableDescriptor => _tableDescriptor;

    public SmallTable(TableDescriptor tableDescriptor, IRecordsIO<TRecord> recordsIO)
    {
        _tableDescriptor = tableDescriptor;
        _recordsIO = recordsIO;
    }

    public void Add(IEnumerable<Record> records)
    {
        foreach(var record in records)
        {
            ExceptionIfRecordIsInvalid(record, _tableDescriptor);
            _recordsIO.Add(record, _tableDescriptor);
        }
    }

    public void Delete(Record record)
    {
        ExceptionIfRecordIsInvalid(record, _tableDescriptor);
        
        var index = _recordsIO.Find(record, _tableDescriptor);

        _recordsIO.Delete(index, _tableDescriptor);
    }

    public void Edit(Record record, Record searchDescriptor)
    {
        ExceptionIfRecordIsInvalid(record, _tableDescriptor);

        uint? index = null;
        foreach(var r in _recordsIO.TableRecordsEnumerator(_tableDescriptor))
        {
            if (MatchByDescriptor(r, searchDescriptor)) {
                index = _recordsIO.Find(r, _tableDescriptor);
                break;
            }
        }

        if (index != null)
        {
            _recordsIO.Edit((uint)index, record, _tableDescriptor);
        }
    }

    public IEnumerable<Record> Get(Record searchDescriptor)
    {
        var result = new List<Record>();

        foreach(var r in _recordsIO.TableRecordsEnumerator(_tableDescriptor))
        {
            if (MatchByDescriptor(r, searchDescriptor))
            {
                result.Add(r);
            }
        }

        return result;
    }

    public IEnumerable<Record> GetAll()
    {
        return _recordsIO.TableRecordsEnumerator(_tableDescriptor);
    }

    private void ExceptionIfRecordIsInvalid(Record record, TableDescriptor tableDescriptor)
    {
        if (!tableDescriptor.IsRecordValid(record))
        {
            throw new TableException("Record doesn't match");
        }
    }

    private bool MatchByDescriptor(Record record, Record descriptor)
    {
        var joinedCells = record
            .Cells
            .Join(descriptor.Cells, 
                x => x.ColumnDescriptor, 
                x => x.ColumnDescriptor, 
                (l, r) => (l, r)).ToArray();

        return joinedCells
            .Aggregate(
                true,
                (prod, next) => 
                    prod 
                    &&
                    (next.r.Value == null ? true : next.l.Value.Equals(next.r.Value)));
    }
}