namespace Sander.SmallDB.Adapters.Json;

using System.Net.Mail;
using System.Text.Json;
using System.Text.Json.Nodes;
using Sander.SmallDB.Contracts.Models;

public static class JsonTypesConverter
{
    public static KeyValuePair<string, JsonNode?> CellToValueJson(Cell cell)
    {
        JsonValue value = cell.ColumnDescriptor.Type switch
        {
            TypeEnum.Integer => (JsonValue?)(int)cell.Value,
            TypeEnum.String => (JsonValue?)(string)cell.Value,
            TypeEnum.Real => (JsonValue?)(double)cell.Value,
            TypeEnum.Char => (JsonValue?)(char)cell.Value,
            TypeEnum.Email => (JsonValue?)(string)(new MailAddress(cell.Value.ToString()).Address),
            _ => throw new NotImplementedException(),
        } ?? throw new JsonException();

        return new KeyValuePair<string, JsonNode?>("value", value);
    }

    public static object JsonNodeToValue(JsonNode? jsonValue, TypeEnum type)
    { 
        object result = type switch
        {
            TypeEnum.Integer => (object?)((int?)jsonValue),
            TypeEnum.String => (object?)(string?)jsonValue,
            TypeEnum.Real => (object?)(double?)jsonValue,
            TypeEnum.Char => (object?)(char?)jsonValue,
            TypeEnum.Email => (object?)(new MailAddress(((string?)jsonValue))),
            _ => throw new NotImplementedException(),
        } ?? throw new JsonException();

        return result;
    }

    public static KeyValuePair<string, JsonNode?> CellToTypeJson(Cell cell)
    {
        JsonValue type = (JsonValue?)cell.ColumnDescriptor.Type.ToString() ?? throw new JsonException();

        return new KeyValuePair<string, JsonNode?>("type", type);
    }


}