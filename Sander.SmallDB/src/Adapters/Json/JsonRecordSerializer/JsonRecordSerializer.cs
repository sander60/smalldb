namespace Sander.SmallDB.Adapters.Json;

using System.Text.Json;
using System.Text.Json.Nodes;

using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Services;

public class JsonRecordSerializer : IRecordSerilizer<JsonObject>
{
    public Record Deserialize(JsonObject source)
    {
        var cells = new List<Cell>();
        foreach(var jsonCell in source)
        {
            cells.Add(JsonNodeToCell(jsonCell));
        }

        return new Record(cells.ToArray());
    }

    public JsonObject Serialize(Record record)
    {
        var result = new JsonObject();
        foreach(var c in record.Cells)
        {
            result.Add(CellToJson(c));
        }
        return result;
    }

    private KeyValuePair<string, JsonNode?> CellToJson(Cell cell)
    {
        JsonNode? cellJson = new JsonObject(new KeyValuePair<string, JsonNode?>[] {
            JsonTypesConverter.CellToTypeJson(cell),
            JsonTypesConverter.CellToValueJson(cell)
        });
        return new KeyValuePair<string, JsonNode?>(
            cell.ColumnDescriptor.Name,
            cellJson
        );
    }

    private Cell JsonNodeToCell(KeyValuePair<string, JsonNode?> json)
    {
        var name = json.Key;
        var values = json.Value ?? throw new JsonException();
        var type = JsonNodeToType(values["type"]);
        object value = JsonTypesConverter.JsonNodeToValue(values["value"], type);

        return new Cell(value, new ColumnDescriptor(type, name));
    }

    private TypeEnum JsonNodeToType(JsonNode? type) => Enum.Parse<TypeEnum>(((string?)type) ?? throw new JsonException());
}
