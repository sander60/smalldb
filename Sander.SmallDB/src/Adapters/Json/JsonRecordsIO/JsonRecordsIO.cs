namespace Sander.SmallDB.Adapters.Json;

using System.Collections.Generic;
using System.Text;
using System.Text.Json.Nodes;

using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Contracts.Models.Exceptions;
using Sander.SmallDB.Contracts.Services;

public class JsonRecordsIO : IRecordsIO<JsonObject>
{
    private SchemeDescriptor _schemeDescriptor;

    private Dictionary<int, List<Record>> _tableRecordsPairs;

    private IRecordSerilizer<JsonObject> _recordSerilizer;

    public JsonRecordsIO() {}

    public void Bind(SchemeDescriptor schemeDescriptor)
    {
        _schemeDescriptor = schemeDescriptor;

        _recordSerilizer = new JsonRecordSerializer();

        _tableRecordsPairs = new Dictionary<int, List<Record>>();

        foreach(var tableDescriptor in _schemeDescriptor.TableDescriptors)
        {
            _tableRecordsPairs.Add(tableDescriptor.GetHashCode(), ReadRecordsFromFileAsync(tableDescriptor));
        }
    }

    public void Add(Record record, TableDescriptor tableDescriptor)
    {
        ExceptionIfRecordIsInvalid(record, tableDescriptor);

        _tableRecordsPairs[tableDescriptor.GetHashCode()].Add(record);

        SaveChanges();
    }

    public void Delete(uint index, TableDescriptor tableDescriptor)
    {
        _tableRecordsPairs[tableDescriptor.GetHashCode()].RemoveAt((int)index);

        SaveChanges();
    }

    public void Edit(uint index, Record record, TableDescriptor tableDescriptor)
    {
        ExceptionIfRecordIsInvalid(record, tableDescriptor);

        _tableRecordsPairs[tableDescriptor.GetHashCode()][(int)index] = record;

        SaveChanges();
    }

    public Record Get(uint index, TableDescriptor tableDescriptor)
    {
        return _tableRecordsPairs[tableDescriptor.GetHashCode()].ElementAt((int)index);
    }

    public uint Find(Record record, TableDescriptor tableDescriptor)
    {
        ExceptionIfRecordIsInvalid(record, tableDescriptor);

        return (uint)_tableRecordsPairs[tableDescriptor.GetHashCode()].IndexOf(record);
    }

    public void SaveChanges()
    {
        foreach(var tableDescriptor in _schemeDescriptor.TableDescriptors)
        {
            WriteRecordsToFile(tableDescriptor);
        }
    }

    public IEnumerable<Record> TableRecordsEnumerator(TableDescriptor tableDescriptor)
    {
        foreach(var record in _tableRecordsPairs[tableDescriptor.GetHashCode()])
        {
            yield return record;
        }
    }

    public void AddTable(TableDescriptor tableDescriptor)
    {
        _schemeDescriptor.TableDescriptors.Add(tableDescriptor);
        _tableRecordsPairs.Add(tableDescriptor.GetHashCode(), new List<Record>());

        SaveChanges(); 
    }

    public void DeleteTable(TableDescriptor tableDescriptor)
    {
        var dbPath = _schemeDescriptor?.DBPath ?? throw new ArgumentNullException();
        var tablePath = Path.Combine(dbPath, tableDescriptor.Name + ".json" ?? throw new ArgumentNullException());

        File.Delete(tablePath);
        _schemeDescriptor.TableDescriptors.Remove(tableDescriptor);
        _tableRecordsPairs.Remove(tableDescriptor.GetHashCode());
    }

    public void RenameTable(TableDescriptor tableDescriptor, string newName)
    {
        var dbPath = _schemeDescriptor?.DBPath ?? throw new ArgumentNullException();
        var tablePath = Path.Combine(dbPath, tableDescriptor.Name + ".json" ?? throw new ArgumentNullException());

        var newTablePath = Path.Combine(dbPath, newName + ".json");

        tableDescriptor.Name = newName;

        File.Move(tablePath, newTablePath);
    }

    private List<Record> ReadRecordsFromFileAsync(TableDescriptor tableDescriptor) 
    {
        var dbPath = _schemeDescriptor?.DBPath ?? throw new ArgumentNullException();
        var tablePath = Path.Combine(dbPath, tableDescriptor.Name + ".json" ?? throw new ArgumentNullException());

        var result = new List<Record>();

        if (!File.Exists(tablePath))
        {
            CreateEmptyTableFile(tablePath);
            return result;
        }

        var JsonString = File.ReadAllText(tablePath);

        var jsonArray = JsonObject.Parse(JsonString)?.AsArray() ?? throw new NullReferenceException();

        foreach(var node in jsonArray)
        {
            result.Add(_recordSerilizer.Deserialize(node?.AsObject() ?? throw new NullReferenceException()));
        }

        return result;
    }

    private void WriteRecordsToFile(TableDescriptor tableDescriptor)
    {
        var dbPath = _schemeDescriptor?.DBPath ?? throw new ArgumentNullException();
        var tablePath = Path.Combine(dbPath, tableDescriptor.Name + ".json" ?? throw new ArgumentNullException());

        if (!File.Exists(tablePath))
        {
            CreateEmptyTableFile(tablePath);
        }

        var jsonRecords = _tableRecordsPairs[tableDescriptor.GetHashCode()]
            .Select(x => _recordSerilizer.Serialize(x))
            .ToArray();

        var jsonArray = new JsonArray();

        foreach(var jsonRecord in jsonRecords)
        {
            jsonArray.Add(jsonRecord);
        }

        var JsonString = jsonArray.ToJsonString();

        File.WriteAllText(tablePath, JsonString);
    }

    private void ExceptionIfRecordIsInvalid(Record record, TableDescriptor tableDescriptor)
    {
        if (!tableDescriptor.IsRecordValid(record))
        {
            throw new TableException("Record doesn't match");
        }
    }

    private void CreateEmptyTableFile(string path)
    {
        var file = File.Create(path);

        file.Write(Encoding.UTF8.GetBytes("[]"));

        file.Close();
    }

    private void UpdateTables()
    {
        foreach(var tableDescriptor in _schemeDescriptor.TableDescriptors)
        {
            if (!_tableRecordsPairs.ContainsKey(tableDescriptor.GetHashCode()))
            {
                _tableRecordsPairs.Add(tableDescriptor.GetHashCode(), ReadRecordsFromFileAsync(tableDescriptor));
            }
        }
    }
}