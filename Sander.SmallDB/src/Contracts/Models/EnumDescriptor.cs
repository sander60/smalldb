namespace Sander.SmallDB.Contracts.Models;

public class EnumDescriptor
{
    public string Name { get; set; }

    public List<string> PosibleValues { get; }

    public EnumDescriptor(string name)
    {
        Name = name;
        PosibleValues = new List<string>();
    }
}