namespace Sander.SmallDB.Contracts.Models;

using System.Reflection;

public class ColumnDescriptor
{
    public TypeEnum Type { get; set; }

    public string Name { get; set; }

    public ColumnDescriptor() { }

    public ColumnDescriptor(TypeEnum type, string name)
    {
        Type = type;
        Name = name;
    }

    public ColumnDescriptor(PropertyInfo propertyInfo)
    {
        var type = propertyInfo.PropertyType;
        var typeEnum = type.ToEnum();
        var propertyName = propertyInfo.Name;

        Type = typeEnum;
        Name = propertyName;
    }

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        return Type == ((ColumnDescriptor) obj).Type &&
        string
            .Equals(Name,
            ((ColumnDescriptor) obj).Name,
            StringComparison.OrdinalIgnoreCase);
    }

    public override int GetHashCode()
    {
        var hashCode = new HashCode();
        hashCode.Add (Name);
        hashCode.Add (Type);
        return hashCode.ToHashCode();
    }
}