namespace Sander.SmallDB.Contracts.Models;

using System.Net.Mail;

public enum TypeEnum
{
    Integer,
    Real,
    String,
    Char,
    Enum,
    Email
}

public static class TypeEnumExtansion {
    public static TypeEnum ToEnum(this Type type) {
        if (type.Equals(typeof(int)) || type.Equals(typeof(int?)))
        {
            return TypeEnum.Integer;
        } 
        else if (type.Equals(typeof(string)))
        {
            return TypeEnum.String;
        }
        else if (type.Equals(typeof(double)) || type.Equals(typeof(double?)))
        {
            return TypeEnum.Real;
        }
        else if (type.Equals(typeof(char)) || type.Equals(typeof(char?)))
        {
            return TypeEnum.Char;
        }
        else if (type.Equals(typeof(MailAddress)))
        {
            return TypeEnum.Email;
        }
        throw new Exception();
    }
}