namespace Sander.SmallDB.Contracts.Models;

using System.Reflection;

public class Cell
{
    public object? Value { get; }

    public ColumnDescriptor ColumnDescriptor { get; }

    public Cell(object value, ColumnDescriptor columnDescriptor)
    {
        Value = value;
        ColumnDescriptor = columnDescriptor;
    }

    public Cell(PropertyInfo propertyInfo, object obj)
    {
        var columnDescriptor = new ColumnDescriptor(propertyInfo);
        object? value = propertyInfo?.GetValue(obj);

        ColumnDescriptor = columnDescriptor;
        Value = value;
    }

    public override bool Equals(object obj)
    {
        if (obj.GetType() != typeof(Cell))
        {
            return false;
        }
        var other = (Cell)obj;
        return 
            other.ColumnDescriptor.Equals(ColumnDescriptor)
            && 
            other.Value.Equals(Value);
    }
    
    // override object.GetHashCode
    public override int GetHashCode()
    {
        var hashCode = new HashCode();

        hashCode.Add(ColumnDescriptor.GetHashCode());
        hashCode.Add(Value.GetHashCode);
        return base.GetHashCode();
    }
}