namespace Sander.SmallDB.Contracts.Models;

using Newtonsoft.Json;

public class SchemeDescriptor
{
    public string Name { get; set; }

    [JsonIgnore]
    public string DBPath { get; set; }

    public List<TableDescriptor> TableDescriptors { get; }

    public List<EnumDescriptor> EnumDescriptors { get; }

    public SchemeDescriptor(string name, string dbPath)
    {
        Name = name;
        DBPath = dbPath;
        TableDescriptors = new List<TableDescriptor>();
        EnumDescriptors = new List<EnumDescriptor>();
    }
}