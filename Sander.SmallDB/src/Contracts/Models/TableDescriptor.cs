namespace Sander.SmallDB.Contracts.Models;

using Sander.SmallDB.Contracts.Models.Exceptions;

public class TableDescriptor
{
    public string? Name { get; set; }

    public ColumnDescriptor[]? ColumnDescriptors { get; set; }

    public TableDescriptor() {}

    public TableDescriptor(string name, ColumnDescriptor[] columnDescriptors)
    {
        Name = name;
        ColumnDescriptors = columnDescriptors;
    }

    public TableDescriptor(Type type): this(type.Name, type) { }

    public TableDescriptor(string name, Type type)
    {
        var properties = type.GetProperties();
        var columnDescriptors = new List<ColumnDescriptor>();
        Name = name;
        foreach (var property in properties)
        {
            columnDescriptors.Add(new ColumnDescriptor(property));
        }
        ColumnDescriptors = columnDescriptors.ToArray();
    }

    public bool IsRecordValid(Record record)
    {
        if (Name == null || ColumnDescriptors == null)
        {
            throw new TableException("Table descriptor is null");
        }
        var recordColumnDescriptors = record.Cells.Select(c => c.ColumnDescriptor).ToList();

        foreach(var c in ColumnDescriptors)
        {
            recordColumnDescriptors.Remove(c);
        }

        return !recordColumnDescriptors.Any();
    }
}