namespace Sander.SmallDB.Contracts.Models;

public class Record
{
    //TODO: make translator types for all needed types
    public Cell[] Cells { get; }

    public Record(object obj)
    {
        var properties = obj.GetType().GetProperties();

        var resultCells = new List<Cell>();
        foreach (var property in properties)
        {
            resultCells.Add(new Cell(property, obj));
        }
        Cells = resultCells.ToArray();
    }

    public Record (Cell[] cells)
    {
        Cells = cells;
    }

    public T ToType<T>()
        where T : new()
    {
        var properties = typeof (T).GetProperties();

        T result = new T();

        foreach (var property in properties)
        {
            var columnDescriptor = new ColumnDescriptor(property);

            var correspondingCell =
                Cells
                    .First(c =>
                        c.ColumnDescriptor.Equals(columnDescriptor));

            property.SetValue(result, correspondingCell.Value);
        }
        return result;
    }

    public override bool Equals(object? obj)
    {
        if (obj?.GetType() != typeof(Record))
        {
            return false;
        }

        var joinedCells = ((Record)obj).Cells
            .Join(this.Cells, 
                x => x.ColumnDescriptor, 
                x => x.ColumnDescriptor, 
                (l, r) => (l, r)).ToArray();
        
        if (joinedCells.Length != this.Cells.Length || joinedCells.Length != ((Record)obj).Cells.Length)
        {
            return false;
        }

        return joinedCells
            .Aggregate(
                true,
                (prod, next) => prod && (next.l.Value.Equals(next.r.Value)));
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Cells);
    }
}