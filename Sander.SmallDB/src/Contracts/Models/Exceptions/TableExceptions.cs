namespace Sander.SmallDB.Contracts.Models.Exceptions;

public class TableException : Exception
{
    public TableException(string? message) :
        base(message)
    {
    }
}