namespace Sander.SmallDB.Contracts.Services;

using Sander.SmallDB.Contracts.Models;

public interface ITable
{
    TableDescriptor TableDescriptor { get; }

    IEnumerable<Record> Get(Record searchDescriptor);

    void Add(IEnumerable<Record> records);

    void Delete(Record record);

    void Edit(Record record, Record searchDescriptor);
    
    IEnumerable<Record> GetAll();    
}