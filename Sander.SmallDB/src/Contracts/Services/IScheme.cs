using Sander.SmallDB.Contracts.Models;

namespace Sander.SmallDB.Contracts.Services;

public interface IScheme<TRecord>
{
    ITable GetTable(string name);

    void AddTable(TableDescriptor tableDescriptor);

    IEnumerable<Record> Join(ITable left, ITable right, string leftKey, string rightString);

    void DeleteTable(string name);

    void ChangeTableName(string before, string after);
    
    void ChangeSchemeName(string name);

    SchemeDescriptor SchemeDescriptor { get; }
}