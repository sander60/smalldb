namespace Sander.SmallDB.Contracts.Services;

using Sander.SmallDB.Contracts.Models;

public interface IRecordsIO<TResult>
{
    Record Get(uint index, TableDescriptor tableDescriptor);

    void Add(Record record, TableDescriptor tableDescriptor);

    void Delete(uint index, TableDescriptor tableDescriptor);

    void Edit(uint index, Record record, TableDescriptor tableDescriptor);

    uint Find(Record record, TableDescriptor tableDescriptor);

    IEnumerable<Record> TableRecordsEnumerator(TableDescriptor tableDescriptor);

    void AddTable(TableDescriptor tableDescriptor);

    void DeleteTable(TableDescriptor tableDescriptor);

    void RenameTable(TableDescriptor tableDescriptor, string newName);

    void Bind(SchemeDescriptor schemeDescriptor);
}