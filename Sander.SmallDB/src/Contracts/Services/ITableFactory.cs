namespace Sander.SmallDB.Contracts.Services;

using Sander.SmallDB.Contracts.Models;

public interface ITableFactory<TRecord>
{
    ITable Create(TableDescriptor tableDescriptor, IRecordsIO<TRecord> recordsIO); 
}