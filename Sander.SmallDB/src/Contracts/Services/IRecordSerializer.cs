namespace Sander.SmallDB.Contracts.Services;

using Sander.SmallDB.Contracts.Models;

public interface IRecordSerilizer<TResult>
{
    TResult Serialize(Record record);

    Record Deserialize(TResult source);
}

