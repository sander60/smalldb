namespace Sander.SmallDB.Server.Controllers.V1;

using System.Linq;
using System.Text.Json.Nodes;
using Microsoft.AspNetCore.Mvc;
using Sander.SmallDB.Adapters.Json;
using Sander.SmallDB.Server.Contracts.Services;

[Route ("/schemes/{scheme_name}/tables/{table_name}/records")]
public class RecordsController : Controller
{
    IDataBaseProvider _dataBaseProvider;
    JsonRecordSerializer _recordSerializer;

    public RecordsController(IDataBaseProvider dataBaseProvider)
    {
        _dataBaseProvider = dataBaseProvider;
        _recordSerializer = new JsonRecordSerializer();
    }

    [HttpPost]
    public ActionResult AddRecord(
        [FromRoute (Name = "scheme_name")] string schemeName,
        [FromRoute (Name = "table_name")] string tableName,
        [FromBody] JsonObject record)
    {
        try
        {
            _dataBaseProvider
                .GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .GetTable(tableName)
                .Add(new []{_recordSerializer.Deserialize(record)});
            return new OkResult();
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }

    [HttpGet]
    public ActionResult GetRecords(
        [FromRoute (Name = "scheme_name")] string schemeName,
        [FromRoute (Name = "table_name")] string tableName)
    {
        try
        {
            var result = _dataBaseProvider
                .GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .GetTable(tableName)
                .GetAll()
                .Select(r => _recordSerializer.Serialize(r))
                .ToArray();
            return new JsonResult(result);
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }

    [HttpDelete]
    public ActionResult DeleteRecord(
        [FromRoute (Name = "scheme_name")] string schemeName,
        [FromRoute (Name = "table_name")] string tableName,
        [FromBody] JsonObject record)
    {
        try
        {
            var table = _dataBaseProvider
                .GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .GetTable(tableName);

            table.Delete(_recordSerializer.Deserialize(record));

            return new OkResult();
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }

    [HttpPut]
    public ActionResult ChangeRecord(
        [FromRoute (Name = "scheme_name")] string schemeName,
        [FromRoute (Name = "table_name")] string tableName,
        [FromBody] JsonObject records)
    {
        try
        {
            var table = _dataBaseProvider
                .GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .GetTable(tableName);
            var before = _recordSerializer.Deserialize((JsonObject)records["before"]);
            var after = _recordSerializer.Deserialize((JsonObject)records["after"]);
            table.Edit(after, before);

            return new OkResult();
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }
}