namespace Sander.SmallDB.Server.Controllers.V1;

using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sander.SmallDB.Contracts.Models;
using Sander.SmallDB.Server.Contracts.Services;

[Route("schemes/{scheme_name}/tables")]
public class TablesController : Controller
{    
    IDataBaseProvider _dataBaseProvider;

    public TablesController(IDataBaseProvider dataBaseProvider)
    {
        _dataBaseProvider = dataBaseProvider;
    }

    [HttpPost]
    public ActionResult CreateTable(
        [FromBody]TableDescriptor tableDescriptor,
        [FromRoute(Name = "scheme_name")] string schemeName)
    {
        try
        {
            _dataBaseProvider.GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .AddTable(tableDescriptor);
            return new OkResult(); 
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }

    [HttpGet]
    public ActionResult GetTables([FromRoute (Name = "scheme_name")] string schemeName)
    {
        try 
        {
            var result = _dataBaseProvider.GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .SchemeDescriptor
                .TableDescriptors
                .Select(t => t.Name);

            return new JsonResult(result);
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }

    [HttpGet("{table_name}")]
    public ActionResult GetTablesDescriptor(
        [FromRoute (Name = "scheme_name")] string schemeName,
        [FromRoute (Name = "table_name")] string tableName)
    {
        try
        {
            var result = _dataBaseProvider.GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .GetTable(tableName)
                .TableDescriptor
                .ColumnDescriptors;

            return new JsonResult(result);
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }

    [HttpPut("{table_name}")]
    public ActionResult RenameTable(
        [FromRoute (Name = "scheme_name")] string schemeName,
        [FromRoute (Name = "table_name")] string tableName,
        string name)
    {
        try
        {
            _dataBaseProvider.GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .ChangeTableName(tableName, name);

            return new OkResult();
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }

    [HttpDelete("{table_name}")]
    public ActionResult DelteTable(
        [FromRoute (Name = "scheme_name")] string schemeName,
        [FromRoute (Name = "table_name")] string tableName)
    {
        try
        {
            _dataBaseProvider.GetSchemes()
                .First(s => string.Equals(s.SchemeDescriptor.Name, schemeName, StringComparison.OrdinalIgnoreCase))
                .DeleteTable(tableName);

            return new OkResult();
        }
        catch (Exception e)
        {
            return new BadRequestResult();
        }
    }
}