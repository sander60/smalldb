namespace Sander.SmallDB.Server.Controllers.V1;

using Microsoft.AspNetCore.Mvc;
using Sander.SmallDB.Server.Contracts.Services;

[Route("schemes")]
public class SchemeController : Controller
{
    IDataBaseProvider _dataBaseProvider;

    public SchemeController(IDataBaseProvider dataBaseProvider)
    {
        _dataBaseProvider = dataBaseProvider;
    }

    [HttpGet]
    public ActionResult GetSchemes()
    {
        return new JsonResult(_dataBaseProvider.GetSchemes().Select(s => s.SchemeDescriptor.Name));
    }

    [HttpPost("create")]
    public ActionResult CreateScheme(string name)
    {
        return new JsonResult(_dataBaseProvider.CreateScheme(name));
    }

    [HttpDelete("{scheme_name}")]
    public ActionResult DeleteScheme([FromRoute(Name = "scheme_name")]string schemeName)
    {
        return new JsonResult(_dataBaseProvider.DeleteScheme(schemeName));
    }

    [HttpPut("{scheme_name}")]
    public ActionResult RenameScheme([FromRoute(Name = "scheme_name")]string schemeName, string name)
    {
        return new JsonResult(_dataBaseProvider.RenameScheme(schemeName, name));
    }
}