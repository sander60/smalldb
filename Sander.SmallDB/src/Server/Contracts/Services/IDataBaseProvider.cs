namespace Sander.SmallDB.Server.Contracts.Services;

using System.Text.Json.Nodes;
using Sander.SmallDB.Contracts.Services;

public interface IDataBaseProvider
{
    IEnumerable<IScheme<JsonObject>> GetSchemes();
 
    bool CreateScheme(string name);

    bool RenameScheme(string scheme, string name);

    bool DeleteScheme(string scheme);
}