namespace Sander.SmallDB.Server.Infrastructure;

using Sander.SmallDB.Server.Contracts.Services;
using Sander.SmallDB.Server.Services;

public static class ServiceCollection
{
    public static void RegisterSmallDbServices(this IServiceCollection services)
    {
        services.AddTransient<IDataBaseProvider, DataBaseProvider>();
    }
}