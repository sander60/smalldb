namespace Sander.SmallDB.Server.Services;

using System.Collections.Generic;
using System.Text.Json.Nodes;
using Sander.SmallDB.Implementations.SmallScheme;
using Sander.SmallDB.Server.Contracts.Services;
using Sander.SmallDB.Adapters.Json;
using Sander.SmallDB.Implementations.SmallTable;
using Sander.SmallDB.Contracts.Services;

public class DataBaseProvider : IDataBaseProvider
{
    const string _path = "Resources";

    public bool CreateScheme(string name)
    {
        try 
        {
            if (Directory.Exists(Path.Combine(_path, name)))
            {
                return false;
            }
            var recordsIO = new JsonRecordsIO();
            var tableFactory = new SmallTableFactory<JsonObject>();
            var scheme = new SmallScheme<JsonObject>(_path, name, recordsIO, tableFactory);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public bool DeleteScheme(string scheme)
    {
        var path = Path.Combine(_path, scheme);
        if(Directory.Exists(path))
        {
            Directory.Delete(path, true);
            return true;
        }
        return false;
    }

    public IEnumerable<IScheme<JsonObject>> GetSchemes()
    {
        return Directory.EnumerateDirectories(_path).Skip(0).Select(p => CreateScheme(Path.GetFileName(p)));

        IScheme<JsonObject> CreateScheme(string name)
        {
            var recordsIO = new JsonRecordsIO();
            var tableFactory = new SmallTableFactory<JsonObject>();
            return new SmallScheme<JsonObject>(_path, name, recordsIO, tableFactory);
        }
    }

    public bool RenameScheme(string scheme, string name)
    {
        try
        {
            var path = Path.Combine(_path, scheme);
            if(Directory.Exists(path))
            {
                var recordsIO = new JsonRecordsIO();
                var tableFactory = new SmallTableFactory<JsonObject>();
                var schemeToRename = new SmallScheme<JsonObject>(_path, scheme, recordsIO, tableFactory);
                schemeToRename.ChangeSchemeName(name);
                return true;
            }
            return false;
        }
        catch(Exception e)
        {
            return false;
        }
    }
}